//function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
//}


function flatten(elements) {
	if(!(Array.isArray(elements))) {
		return "";
	}
	const flatArray = [];
	for( let i = 0; i < elements.length; i++) {
		if(Array.isArray(elements[i])) {
			flatArray.push(...flatten(elements[i]));
		}
		else {
			flatArray.push(elements[i]);
		}
	}
	return flatArray;
}

module.exports = flatten;
